import Vue from 'vue'
import Router from 'vue-router'
import viewIndex from '../views/index.vue'
import login from '../login/index'
import noFound from '../component/onFound/404.vue'
const routers = [
  {
    path:"/",
    redirect: 'login'
  },
  {
    path: '/login',
    component: login
  },
  {
    path: '/index',
    component: viewIndex
  },
  {
    path: '/:id',
    component: noFound
  }
]
Vue.use(Router)
const router =  new Router({
  mode:"history",//hash history
  routes: routers,
  linkActiveClass: 'active-class', // 自定义class 作为 路由样式
  linkExactActiveClass: 'init-class',
  scrollBehavior (to, from, savedPosition) {
    console.log(to, from , savedPosition)
    // return 期望滚动到哪个的位置
    if(savedPosition) {
      return savedPosition;
    }else{
      return {x:0,y:0}
    }
  }
})

router.beforeEach((to, from, next) => {
  // console.log(to,from)

   var token = sessionStorage.getItem('token');
   console.log(token)
   if(to.path !== '/login' && token !== null) {
     next()
   }else if(to.path === '/login' && token !== null){
     next({path:'/index'})
   }else if (to.path === '/login' && token === null){
     next()
   }
   else{
     next('/login')
   }
  next()

});
export default router;