export const url = function (name) {
    return () => import(`../views/${name}`)
  }
  import {
    Message
  } from 'element-ui';`在这里插入代码片`
  let messageInstance = null;
  const resetMessage = (options) => {
    if(messageInstance) {
        messageInstance.close()
    }
    messageInstance = Message(options)
    console.log(messageInstance) // 虚拟 vue 组件
  };
  ['error','success','info','warning'].forEach(type => {
    resetMessage[type] = options => {
        if(typeof options === 'string') {
            options = {
                message:options
            }
        }
        options.type = type
        return resetMessage(options)
    }
  })
  export const message = resetMessage