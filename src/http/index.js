import axios from 'axios'
import {message} from '../common/utils'
axios.interceptors.request.use(success => {
  success.headers['token'] = sessionStorage.getItem('token') || ''
  return success
}, err => {
  return err
})
axios.interceptors.response.use(success => {
  // message.error('header',success.headers)
  document.cookie = `token=${success.headers['setcookie']}`
  return success 
  // 此处的数据 是全局调用axios的结构 你可以做处理 比如后端用比较复杂的格式
  // 你可以在这里把复杂的格式 处理成简单的
}, err => {
  if(err && err.response){
    switch(err.response.status){
      case 400:
        message.error("错误请求")
      break
      case 401:
        message.error("未授权，请重新登录")
      break
      case 403:
        message.error("拒绝访问")
      break
      case 404:
        message.error("请求错误，未找到该资源")
      break
      case 408:
        message.error("请求超时")
      break
      case 500:
        message.error("服务器端出错")
      break
      case 501:
        message.error("网络未实现")
      break
      case 502:
        message.error("网络错误")
      break
      case 503:
        message.error("服务不可用")
      break
      case 504:
        message.error("网络超时")
      break
      case 505:
        message.error("http版本不支持该请求")
      break
      default:
        message.error(`连接错误${err.response.status}`)
    }
  }else{
    message.error('连接到服务器失败')
  }
  return err
})
let cancelToken = axios.CancelToken
/** 请求 限制自定义头*/
let axiosOne = axios.create({ // 实例化 用于多种请求
  timeout: 3000,
  url: '',
  baseURL: '',
  method: 'get',
  params: '',
  data: '',
  responseType: 'json',
  cancelToken: new cancelToken(function(){}),
  headers: {
    name: 1
  },
  withCredentials: false,
  onUploadProgress: (progressEvent) => {
    let complete = (progressEvent.loaded / progressEvent.total * 100 | 0) + '%'
  }
})

class Http {
  cancelToken = false // false不取消发送 true 取消发送
  self = this
  header = {
    timeout: 3000, // 延迟
    url: '', // 可用可不用
    baseURL: '', // 
    method: 'get', // 方法
    params: '', // get 参数
    data: '', // post 参数
    responseType: 'json', // 响应的结果
    cancelToken: new cancelToken(function(){}),
    headers: {
      name: 1 // 自定义请求头
    },
    withCredentials: false, // 传cookie true
    onUploadProgress: (progressEvent) => { // 文件上传进度
      let complete = (progressEvent.loaded / progressEvent.total * 100 | 0) + '%'
    }
  }
 
  constructor() {
    this.get = this.getInfo // 发生get方法
    this.post = this.postInfo // post 
    this.fileupload = this.fileuploadInfo // 文件上传
    this.fileUploadProgress = this.fileProgress // 文件进度
  }
  cancal() {
    this.header.cancelToken = new cancelToken(function (res) {
      res('取消发送')
    })
  }
  fileProgress(url, data, header,onUploadProgress,cancal = false) {
    if (cancal) {
      this.cancal()
    }
    let allHeader = JSON.parse(JSON.stringify(Object.assign({},{...this.header.headers},{...header})))
    let headers = {headers:{...allHeader,"content-type": "multipart/form-data"}}
    if(onUploadProgress) {
      this.header.onUploadProgress = onUploadProgress.onUploadProgress
    }
    return axios.post(url, data,Object.assign({},{...this.header},{...headers}))
  }
  getInfo(url, data, cancal = false) { // 如果有中断请求的需求 就 传入true
    if (cancal) {
      this.cancal()
    }
    this.header.url = url
    this.header.params = data
    this.header.method = 'get'
    return axios({
      ...this.header
    })
  }
  postInfo(url, data, cancal = false) {
    if (cancal) {
      this.cancal()
    }
    this.header.url = url
    this.header.data = data
    this.header.method = 'post'
    return axios({
      ...this.header
    })
  }
  fileuploadInfo(url, form, cancal = false) {
    if (cancal) {
      this.cancal()
    }
    this.header.url = url
    this.header.data = form
    this.header.method = 'post'
    this.header.responseType = 'json'
    return axios({
      ...this.header
    })
  }
  imgDown(url,data,cancal = false) { // 图片上传
    if(cancal) {
      this.cancal()
    }
    this.header.url = url
    this.header.data = data
    this.header.method = 'get'
    this.header.responseType = 'blob'
    return axios({
      ...this.header
    })
  }
}
const api = new Http()
export default api